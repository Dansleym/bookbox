<?php
use PHPUnit\Framework\TestCase;
require 'User.php';
class TestUser extends TestCase
{
    public $user;

    protected function setUp() : void
    {
        $this->user = new User();
        $this->user->setAge(33);
    }
    protected function tearDown() : void
    {
        
    }


    public function testAge(){
        $this->assertEquals(33, $this->user->getAge());
    }
}

