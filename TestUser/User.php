<?php
class User
{
    private $name;
    private $age;

    public function __construct($name = null, $age = null){
        $this->name = $name;
        $this->age = $age;
    }

    public function getAge(){
        return $this->age;
    }

    public function setAge($age){
        $this->age = $age;
    }

}

class UserTest
{
    private $user;

    protected function setUp() : void
    {
        $this->user = new User();
        $this->user->setAge(33);
    }
    protected function tearDown() : void
    {
        
    }


    public function testAge(){
        $this->assertEquals(33, $this->user->getAge());
    }
}